# OLIP Deploy

A Docker Compose setup to deploy OLIP.

## Table of Contents

- [Scripts](#scripts)
  - [Installation](#installation)
    - [Installation script usage](#installation-script-usage)
  - [Deploy](#deploy)
    - [Deploy script usage](#deploy-script-usage)
    - [Deploy on Maestro (virtual machine)](#deploy-on-maestro-virtual-machine)
  - [Development](#development)
    - [Development script usage](#development-script-usage)
  - [Reset](#reset)
    - [Reset script usage](#reset-script-usage)
- [Applications integration](#applications-integration)

## Scripts

Deploy repository comes with some scripts to help you out deploy/develop.

### Installation

The installation script makes sure that you have all needed environment variables.

It will also pre-download all needed Docker images.

#### Installation script usage

```text
Usage: install.sh [-t|--tag TAG] [-d|--dev] [-h|--help]
Example: install.sh -h

Options:
  -t, --tag           Target tag to deploy (default: latest)
  -d, --dev           Install for dev purposes
  -h, --help          Display this help message
```

On a production Ideascube you can run :

```shell
bash scripts/install.sh
```

On a staging Ideascube :

```shell
bash scripts/install.sh -t staging
```

For development purposes :

```shell
bash scripts/install.sh -t staging -d
```

### Deploy

The deploy script is used to deploy a virtual machine save from Maestro.

The script will retrieve database and images, and configure environment files.

#### Deploy script usage

```text
Usage: deploy.sh [-v|--vm-save-id VM_SAVE_ID] [-l|--language LANGUAGE] [-s|--ssid SSID] [-i|--interface INTERFACE] [-b|--bsf-core] [-h|--help]
Example: deploy.sh -v VM_SAVE_ID -l eng -s  -i wlan0

Options:
  -b, --bsf-core      Use BSF Core
  -h, --help          Display this help message
  -i, --interface     Network interface to use for the hotspot (default: wlan0)
  -l, --language      Default language for OLIP Dashboard (default: eng)
  -s, --ssid          SSID for hotspot
  -v, --vm-save-id    Virtual machine save ID to deploy (required)
  --skip-download     Skip virtual machine database/images download
```

On an Ideascube, `maestro-socket` should handle the deploy command by using deploy from Maestro.

You can still manually deploy it :

```shell
bash scripts/deploy.sh -v VM_SAVE_ID
```

#### Deploy on Maestro (virtual machine)

You will only need to use, after installation done, a docker compose command :

```shell
docker compose -f composes/vm.compose.yml --env-file envs/.env up -d
```

### Development

To prepare your development environment, you need to run install in dev mode.

Here we use staging environment :

```shell
sudo bash scripts/install.sh -d -t staging
```

This is the moment to [configure](#environment-configuration) your environment files.

When it's done, you can run dev script that will run the stack.

```shell
sudo bash scripts/dev.sh up
```

After that, you will need to configure `.dev.env` file.

#### Environment configuration

First thing to do is configuring `.s3fs.env`.

You can now configure `.dev.env` file.

**To keep in mind** :

- `.dev.env` overrides every other env files
- All containers will use configuration files in `envs/` even in development mode

#### Development script usage

```text
Usage: dev.sh COMMAND [-b|--bsf-core] [-h|--help]
Example: dev.sh up

Commands:
  up                  Start all services
  down                Stop all services

Options:
  -b, --bsf-core      Use BSF core
  -h, --help          Display this help message
```

### Reset

The reset script helps you to erase data/configuration of your OLIP deployment.

#### Reset script usage

```shell
Usage: reset.sh [-k|--keep-data] [-d|--dev] [-h|--help]
Example: reset.sh -k

Options:
  -k, --keep-data     Prevent OLIP data deletion (database/images/volumes)
  -d, --dev           Only remove volumes to reset dev environment (keep configuration)
  -h, --help          Display this help message
```

When using development environment, you might want to erase OLIP data to reset database, elasticsearch, ...

You should run this command :

```shell
sudo bash scripts/reset.sh -d
```

## Applications integration

You might need a way to test your application inside of OLIP.

Just follow [development steps](#development).

After that you have to put all your application files into `integration/application`.

Set `APPLICATION_INTEGRATION` variable to `true` in `.dev.env`.

You can start OLIP using the [dev.sh](#development-script-usage) script.
