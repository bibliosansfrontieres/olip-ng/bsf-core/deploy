FROM alpine:3.19

RUN apk add --no-cache \
    git=2.43.5-r0 \
    docker-cli=25.0.5-r1 \
    docker-cli-compose=2.23.3-r3 \
    bash=5.2.21-r0 \
    curl=8.9.1-r1 \
    unzip=6.0-r14

RUN wget https://github.com/mikefarah/yq/releases/download/v4.44.3/yq_linux_amd64 -O /usr/bin/yq &&\
    chmod +x /usr/bin/yq

WORKDIR /home/ideascube/deploy

COPY . .

CMD ["tail", "-f", "/dev/null"]