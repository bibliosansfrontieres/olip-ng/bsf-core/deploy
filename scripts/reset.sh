#!/bin/bash

OLIP_FILES_TO_DELETE="virtual-machine-save-id context status bsf-core deploy"
ENV_FILES_TO_KEEP=".env .hao.env .maestro-socket.env .dev.env"
CONF_FILES_TO_KEEP=""
VOLUME_PATHS_TO_KEEP="DEPLOY_REPOSITORY_VOLUME"
PREFIX="${PREFIX:-}"

KEEP_DATA="${KEEP_DATA:-false}"
DEV="${DEV:-false}"

say() {
    echo "$1"
}

check_sudo() {
    if [ "$(id -u)" -ne 0 ]; then
        say "This script must be run with sudo."
        exit 1
    fi
}

show_help() {
    echo "Usage: $0 [-k|--keep-data] [-d|--dev] [-h|--help]"
    echo "Example: $0 -k"
    echo
    echo "Options:"
    echo "  -k, --keep-data     Prevent OLIP data deletion (database/images/volumes)"
    echo "  -d, --dev           Only remove volumes to reset dev environment (keep configuration)"
    echo "  -h, --help          Display this help message"
    exit 0
}

parse_arguments() {
    while [ $# -gt 0 ]; do
        case "$1" in
            -k|--keep-data)
                KEEP_DATA="true"
                shift
                ;;
            -d|--dev)
                DEV="true"
                shift
                ;;
            -h|--help)
                show_help
                ;;
            *)
                echo "Invalid option: $1"
                exit 1
                ;;
        esac
    done

    echo "TAG: $TAG"
}

get_inside_deploy_repository() {
    SCRIPT_PATH="$(dirname "$(realpath "$0")")"
    DEPLOY_PATH="$(realpath "$SCRIPT_PATH/..")"
    say "Changing directory to \"$DEPLOY_PATH\""
    cd "$DEPLOY_PATH" || exit
}

get_compose_cmd() {
    olip_compose_path="\$HOST_DEPLOY_PATH/composes/ideascube.compose.yml"
    bsf_core_compose_path="\$HOST_DEPLOY_PATH/composes/bsf-core.override.compose.yml"
    compose_default_args="--env-file \"\$HOST_DEPLOY_PATH/envs/.env\" --profile deploy"

    if [ "$BSF_CORE" = "true" ]; then
        compose_cmd="docker compose -f \"$olip_compose_path\" -f \"$bsf_core_compose_path\" $compose_default_args"
    else
        compose_cmd="docker compose -f \"$olip_compose_path\" $compose_default_args"
    fi

    echo "$compose_cmd"
}

check_compose_sh() {
    if [ ! -f "scripts/compose.sh" ]; then
        say "scripts/compose.sh not found!"
        exit 1
    fi
}

get_compose_services() {
    yq e ".services | to_entries | map(select(.value.profiles == null)) | .[].key" "$1"
}

# Get all services from the compose files in INSTALL_COMPOSES that do not have a profile specified
get_install_compose_services() {
    # shellcheck source=/dev/null
    . constants/install-composes
    combined_services=""
    for compose_file in "${INSTALL_COMPOSES[@]}"; do
        combined_services="$combined_services $(get_compose_services "composes/$compose_file")"
    done
    unique_services=$(echo "$combined_services" | tr ' ' '\n' | sort | uniq | tr '\n' ' ')
    echo "$unique_services"
}

list_all_services() {
    TEMP_HOST_DEPLOY_PATH="$HOST_DEPLOY_PATH"
    export HOST_DEPLOY_PATH="$DEPLOY_PATH"
    eval "$(get_compose_cmd) config --services"
    HOST_DEPLOY_PATH="$TEMP_HOST_DEPLOY_PATH"
}

filter_specified_services() {
    all_services=$(list_all_services)
    excluded_services="$*"

    filtered_services=""
    for service in $all_services; do
        found=0
        for excluded in $excluded_services; do
            if [ "$service" = "$excluded" ]; then
                found=1
                break
            fi
        done
        if [ "$found" -eq 0 ]; then
            filtered_services="$filtered_services $service"
        fi
    done

    echo "$filtered_services" | sed 's/^ *//;s/ *$//'
}

get_services_to_stop() {
    filter_specified_services "$(get_install_compose_services)"
}

compose_down() {
    if [ "$DEV" = "true" ]; then
        say "Skip compose down as DEV is true"
        return
    fi

    check_compose_sh
    say "Stopping OLIP stack"
    services_to_stop="$(get_services_to_stop)"
    say "Services to stop: $services_to_stop"
    HOST_DEPLOY_PATH=${DEPLOY_PATH} bash scripts/compose.sh down $services_to_stop
}

remove_docker_volumes() {
    if [ "$KEEP_DATA" = "true" ]; then
        say "Skip removing Docker volumes as KEEP_DATA is true"
        return
    fi

    say "Removing Docker volumes"
    docker volume rm "$(docker volume ls -q)"
}

remove_volume_paths() {
    if [ "$KEEP_DATA" = "true" ]; then
        say "Skip removing volume paths as KEEP_DATA is true"
        return
    fi
    
    say "Removing volume paths"
    env_file="envs/.env"

    while IFS='=' read -r var value; do
        if echo "$var" | grep -q '_VOLUME$'; then
            if echo "$VOLUME_PATHS_TO_KEEP" | grep -q -w "$var"; then
                continue
            fi
            
            if [ -n "$value" ] && [ -d "$value" ]; then
                rm -rf "$value/*"
            fi
        fi
    done < "$env_file"
}

get_env_files() {
    env_files=$(find . -type f -name "*.env")
    filtered_env_files=""
    
    for file in $env_files; do
        should_keep=false
        for keep_file in $ENV_FILES_TO_KEEP; do
            if [ "$(basename "$file")" = "$keep_file" ]; then
                should_keep=true
                break
            fi
        done
        
        if [ "$should_keep" = false ]; then
            filtered_env_files="$filtered_env_files $file"
        fi
    done
    
    echo "$filtered_env_files"
}

get_conf_files() {
    conf_files=$(find . -type f -name "*.conf")
    filtered_conf_files=""
    
    for file in $conf_files; do
        should_keep=false
        for keep_file in $CONF_FILES_TO_KEEP; do
            if [ "$(basename "$file")" = "$keep_file" ]; then
                should_keep=true
                break
            fi
        done
        
        if [ "$should_keep" = false ]; then
            filtered_conf_files="$filtered_conf_files $file"
        fi
    done
    
    echo "$filtered_conf_files"
}

remove_configuration() {
    if [ "$DEV" = "true" ]; then
        say "Skip removing configuration as DEV is true"
        return
    fi

    vm_save_id=$(cat /olip-files/deploy/virtual-machine-save-id)
    backup_directory="$PREFIX/olip-backups/deploy-$vm_save_id-$(date +%s)/"
    mkdir -p "$backup_directory"

    say "Removing env files"
    target_directory="$backup_directory/envs/"
    mkdir -p "$target_directory"
    for env_file in $(get_env_files); do
        target_file="$target_directory$(basename "$env_file")"
        if [ -f "$target_file" ]; then
            rm -f "$target_file"
        fi
        mv -f "$env_file" "$target_file"
    done

    say "Removing conf files"
    target_directory="$backup_directory/confs/"
    mkdir -p "$target_directory"
    for conf_file in $(get_conf_files); do
        target_file="$target_directory$(basename "$conf_file")"
        if [ -f "$target_file" ]; then
            rm -f "$target_file"
        fi
        mv -f "$conf_file" "$target_file"
    done

    say "Removing olip-files"
    target_directory="$backup_directory/olip-files/"
    mkdir -p "$target_directory"

    cp -r /olip-files/* "$target_directory"
    for olip_file in $OLIP_FILES_TO_DELETE; do
        if [ -e "/olip-files/$olip_file" ]; then
            rm -rf "/olip-files/$olip_file"
        fi
    done
}

check_sudo
parse_arguments "$@"
get_inside_deploy_repository
compose_down
remove_docker_volumes
remove_volume_paths
remove_configuration
exit 0