#!/bin/bash

set -euo pipefail

LANGUAGE="eng"
INTERFACE="wlan0"
BSF_CORE="false"
SKIP_DOWNLOAD="false"
SSID=""
STANDALONE=false
BRANCH=false
OLIP_FILES_PATH=/olip-files
OLIP_DATA_PATH=/olip

SCRIPT_PATH="$(dirname "$(realpath "$0")")"
DEPLOY_PATH="$(realpath "$SCRIPT_PATH/..")"
HOST_DEPLOY_PATH="$DEPLOY_PATH"

ENV_FILES_TO_KEEP=".env .maestro-socket.env .hao.env .dev.env"

say() {
    echo "$1"
}

show_help() {
    echo "Usage: $0 [-v|--vm-save-id VM_SAVE_ID] [-l|--language LANGUAGE] [-s|--ssid SSID] [-i|--interface INTERFACE] [-b|--bsf-core] [-h|--help]"
    echo "Example: $0 -v VM_SAVE_ID -l $LANGUAGE -i $INTERFACE"
    echo
    echo "Options:"
    echo "  -b, --bsf-core      Use BSF Core"
    echo "  -h, --help          Display this help message"
    echo "  -i, --interface     Network interface to use for the hotspot (default: $INTERFACE)"
    echo "  -l, --language      Default language for OLIP Dashboard (default: $LANGUAGE)"
    echo "  -s, --ssid          SSID for hotspot"
    echo "  -v, --vm-save-id    Virtual machine save ID to deploy (required)"
    echo "  -o, --olip-files    Specify olip-files path"
    echo "  -O, --olip-data     Specify olip path"
    echo "  --skip-download     Skip virtual machine database/images download"
    echo "  --standalone        Use standalone mode"
    echo "  --branch            Use branch mode"
    echo "  --deploy-path       Specify real deploy path (usefull for using this script inside docker)"
    exit 0
}

parse_arguments() {
    while [ $# -gt 0 ]; do
        case "$1" in
            -v|--vm-save-id)
                VM_SAVE_ID="$2"
                shift 2
                ;;
            -o|--olip-files)
                OLIP_FILES_PATH="$2"
                shift 2
                ;;
            -O|--olip-data)
                OLIP_DATA_PATH="$2"
                shift 2
                ;;
            -l|--language)
                LANGUAGE="$2"
                shift 2
                ;;
            -s|--ssid)
                SSID="$2"
                shift 2
                ;;
            -i|--interface)
                INTERFACE="$2"
                shift 2
                ;;
            -b|--bsf-core)
                BSF_CORE="true"
                shift
                ;;
            --skip-download)
                SKIP_DOWNLOAD="true"
                shift
                ;;
            --standalone)
                if [ "$BRANCH" = "true" ]; then
                    say "Error: --branch and --standalone cannot be used together"
                    exit 1
                fi
                STANDALONE="true"
                shift
                ;;
            --branch)
                if [ "$STANDALONE" = "true" ]; then
                    say "Error: --branch and --standalone cannot be used together"
                    exit 1
                fi
                BRANCH="true"
                shift
                ;;
            --deploy-path)
                HOST_DEPLOY_PATH="$2"
                shift 2
                ;;
            -h|--help)
                show_help
                ;;
            *)
                echo "Invalid option: $1"
                exit 1
                ;;
        esac
    done
}

source_env_file() {
    # shellcheck source=/dev/null
    source envs/.env
}

validate_vm_save_id(){
    [ -n "$VM_SAVE_ID" ] || { echo >&2 "Error: VM_SAVE_ID is mandatory." ; exit 12 ; }
    http_code=$( curl -s -o /dev/null -w "%{http_code}" --head "$MAESTRO_API_URL/virtual-machine-saves/$VM_SAVE_ID/database" )
    if [ "$http_code" == "404" ] ; then
        echo >&2 "Error: Maestro 404'ed for VM_SAVE_ID ${VM_SAVE_ID}." \
        exit 44
    fi
}

get_inside_deploy_repository() {
    echo "Changing directory to \"$DEPLOY_PATH\""
    cd "$DEPLOY_PATH" || exit 1
}

write_olip_files() {
    mkdir -p "${OLIP_FILES_PATH}/deploy"

    echo "$VM_SAVE_ID" > "${OLIP_FILES_PATH}/deploy/virtual-machine-save-id"
    echo "$INTERFACE" > "${OLIP_FILES_PATH}/deploy/interface"
    echo "$LANGUAGE" > "${OLIP_FILES_PATH}/deploy/language"

    if [ -n "$SSID" ]; then
        echo "$SSID" > "${OLIP_FILES_PATH}/deploy/ssid"
    fi

    if [ "$BSF_CORE" = "true" ]; then
        echo "true" > "${OLIP_FILES_PATH}/deploy/bsf-core"
    fi
}

configure_env_and_conf_files() {
    if [ -n "$VM_SAVE_ID" ]; then
        sed -i "s/^VM_SAVE_ID=.*/VM_SAVE_ID=$VM_SAVE_ID/" "envs/.olip-api.env"
        sed -i "s@^MAESTRO_WEBSOCKET_API_URL=.*@MAESTRO_WEBSOCKET_API_URL=http://172.17.0.1:9191/@" "envs/.olip-api.env"
    fi

    if [ -n "$LANGUAGE" ]; then
        sed -i "s/^VITE_DEFAULT_LANGUAGE=.*/VITE_DEFAULT_LANGUAGE=$LANGUAGE/" "envs/.olip-dashboard.env"
    fi

    if [ -n "$SSID" ]; then
        sed -i "s/^ssid=.*/ssid=$SSID/" "confs/hostapd.conf"
    fi

    if [ -n "$INTERFACE" ]; then
        sed -i "s/^interface=.*/interface=$INTERFACE/" "confs/hostapd.conf"
        sed -i "s/^interface=.*/interface=$INTERFACE/" "confs/dnsmasq.conf"
    fi

    if [ -f "envs/.deploy.env" ]; then
        while IFS='=' read -r key value; do
            # Skip empty lines or lines starting with #
            if [[ -z "$key" || "$key" == \#* ]]; then
                continue
            fi
            # Trim any leading/trailing whitespace
            key=$(echo "$key" | xargs)
            value=$(echo "$value" | xargs)

            for env_file in envs/.?*.env; do
                if grep -q "^${key}=" "$env_file"; then
                    sed -i "s|^${key}=.*|${key}=${value}|" "$env_file"
                fi
            done

            for conf_file in confs/.?*.conf; do
                if grep -q "^${key}=" "$conf_file"; then
                    sed -i "s|^${key}=.*|${key}=${value}|" "$conf_file"
                fi
            done
        done < "envs/.deploy.env"
    fi
}

import_database() {
    if [ "$SKIP_DOWNLOAD" = "true" ]; then
        return
    fi

    say "Importing database"

    database_dir="${OLIP_DATA_PATH}/databases"
    mkdir -p "$database_dir"
    curl -s -o "$database_dir/olip-api-v2.db" "$MAESTRO_API_URL/virtual-machine-saves/$VM_SAVE_ID/database"
}

import_context() {
    if [ "$SKIP_DOWNLOAD" = "true" ]; then
        return
    fi

    say "Importing context"

    context_dir="${OLIP_FILES_PATH}/deploy"
    mkdir -p $context_dir
    curl -s -o "$context_dir/context" "$MAESTRO_API_URL/virtual-machine-saves/$VM_SAVE_ID/context"
    echo -e "\n\r" >> "$context_dir/context"
}

import_images() {
    if [ "$SKIP_DOWNLOAD" = "true" ]; then
        return
    fi

    say "Importing images"

    images_dir="${OLIP_DATA_PATH}/static/images"
    mkdir -p "$images_dir"
    zip_file="images.zip"
    curl -s -o "$zip_file" "$MAESTRO_API_URL/virtual-machine-saves/$VM_SAVE_ID/images"
    unzip -o "$zip_file" -d "$images_dir"
    rm -f "$zip_file"
}

get_compose_cmd() {
    olip_compose_path="\$HOST_DEPLOY_PATH/composes/ideascube.compose.yml"
    branch_compose_path="\$HOST_DEPLOY_PATH/composes/branch.compose.yml"
    standalone_compose_path="\$HOST_DEPLOY_PATH/composes/standalone.compose.yml"
    bsf_core_compose_path="\$HOST_DEPLOY_PATH/composes/bsf-core.override.compose.yml"
    compose_default_args="--env-file \"\$HOST_DEPLOY_PATH/envs/.env\" --profile deploy"
    compose_standalone_args="--env-file \"\$HOST_DEPLOY_PATH/envs/.env\" --env-file \"\$HOST_DEPLOY_PATH/envs/.standalone.env\""

    if [ "$BSF_CORE" = "true" ]; then
        compose_cmd="docker compose -f \"$olip_compose_path\" -f \"$bsf_core_compose_path\" $compose_default_args"
    else
        compose_cmd="docker compose -f \"$olip_compose_path\" $compose_default_args"
    fi

    if [ "$BRANCH" = "true" ]; then
        compose_cmd="docker compose -f \"$branch_compose_path\" $compose_standalone_args"
    fi

    if [ "$STANDALONE" = "true" ]; then
        compose_cmd="docker compose -f \"$standalone_compose_path\" $compose_standalone_args"
    fi

    echo "$compose_cmd"
}

write_compose_script() {
    say "Writing compose script"
    compose_cmd=$(get_compose_cmd)
    echo -e "#!/bin/bash\n\nHOST_DEPLOY_PATH=\${HOST_DEPLOY_PATH:-$HOST_DEPLOY_PATH}\n\n$compose_cmd \"\$@\"" > scripts/compose.sh
    chmod +x scripts/compose.sh
}

get_compose_services() {
    yq e ".services | to_entries | map(select(.value.profiles == null)) | .[].key" "$1"
}

get_install_compose_services() {
    # shellcheck source=/dev/null
    . constants/install-composes
    combined_services=""
    for compose_file in "${INSTALL_COMPOSES[@]}"; do
        combined_services="$combined_services $(get_compose_services "composes/$compose_file")"
    done
    unique_services=$(echo "$combined_services" | tr ' ' '\n' | sort | uniq | tr '\n' ' ')
    echo "$unique_services"
}

list_all_services() {
    TEMP_HOST_DEPLOY_PATH="$HOST_DEPLOY_PATH"
    export HOST_DEPLOY_PATH="$DEPLOY_PATH"
    eval "$(get_compose_cmd) config --services"
    HOST_DEPLOY_PATH="$TEMP_HOST_DEPLOY_PATH"
}

filter_specified_services() {
    all_services=$(list_all_services)
    excluded_services="$*"

    filtered_services=""
    for service in $all_services; do
        found=0
        for excluded in $excluded_services; do
            if [ "$service" = "$excluded" ]; then
                found=1
                break
            fi
        done
        if [ "$found" -eq 0 ]; then
            filtered_services="$filtered_services $service"
        fi
    done

    echo "$filtered_services" | sed 's/^ *//;s/ *$//'
}

get_services_to_start() {
    services_to_filter="$(get_install_compose_services)"
    filter_specified_services "$services_to_filter"
}

docker_compose_up() {
    say "Starting docker compose"
    services_to_start="$(get_services_to_start)"
    say "Services to start: $services_to_start"
    HOST_DEPLOY_PATH=${DEPLOY_PATH} bash scripts/compose.sh up -d --force-recreate --pull always $services_to_start
}

docker_image_prune() {
    say "Pruning Docker system"
    docker image prune --all --force
}

parse_arguments "$@"
get_inside_deploy_repository
source_env_file
validate_vm_save_id
write_olip_files
configure_env_and_conf_files
import_database
import_images
import_context
write_compose_script
docker_compose_up
docker_image_prune