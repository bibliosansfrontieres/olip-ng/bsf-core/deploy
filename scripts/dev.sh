#!/bin/bash

DEV_COMPOSE_TEMPLATE="dev/templates/dev.template.compose.yml"
DEV_COMPOSE="dev/dev.compose.yml"
BSF_CORE_DEV_COMPOSE_TEMPLATE="dev/templates/dev.bsf-core.template.override.compose.yml"
BSF_CORE_DEV_COMPOSE="dev/dev.bsf-core.override.compose.yml"
DEV_ENVIRONMENT_FILE="envs/.dev.env"
MAIN_ENVIRONMENT_FILE="envs/.env"

SCRIPT_PATH="$(dirname "$(realpath "$0")")"
DEPLOY_PATH="$(realpath "$SCRIPT_PATH/..")"

say() {
    echo "$1"
}

is_bsf_core() {
    if [[ -n "$BSF_CORE" && "$BSF_CORE" == "true" ]]; then
        return 0
    fi
    return 1
}

show_help() {
    say "Usage: $0 COMMAND [OPTIONS]"
    say "Example: $0 up"
    say
    say "Commands:"
    say "  up                  Start all services"
    say "  down                Stop all services"
    say
    say "Options:"
    say "  -b, --bsf-core      Use BSF core"
    say "  -h, --help          Display this help message"
    exit 0
}

check_sudo() {
    if [[ "$(id -u)" -ne 0 ]]; then
        say "This script must be run with sudo."
        exit 1
    fi
}

get_inside_deploy_repository() {
    say "Changing directory to \"$DEPLOY_PATH\""
    cd "$DEPLOY_PATH" || exit 1
}

remove_s3fs_bucket() {
    say "Unmounting and removing bucket \"$S3FS_BUCKET_PATH\""
    sudo fusermount -u "$S3FS_BUCKET_PATH" >/dev/null 2>&1 || true
    sudo rm -rf "$S3FS_BUCKET_PATH" >/dev/null 2>&1 || true
}

parse_arguments() {
    COMMAND="$1"
    shift

    if [[ "$COMMAND" != "up" && "$COMMAND" != "down" ]]; then
        say "Error: COMMAND must be 'up' or 'down'."
        show_help
    fi

    while [[ $# -gt 0 ]]; do
        case "$1" in
            -b|--bsf-core)
                BSF_CORE="true"
                shift
                ;;
            -h|--help)
                show_help
                ;;
            *)
                say "Invalid option: $1"
                exit 1
                ;;
        esac
    done
}

source_env_file() {
    say "Sourcing \"$1\""
    if [[ ! -f "$1" ]]; then
        say "\"$1\" file not found."
        exit 1
    fi
    # shellcheck source=/dev/null
    . "$1"
}

source_env_files() {
    source_env_file "$MAIN_ENVIRONMENT_FILE"
    source_env_file "$DEV_ENVIRONMENT_FILE"
}

copy_dev_compose_templates() {
    cp "$DEV_COMPOSE_TEMPLATE" "$DEV_COMPOSE"
    if is_bsf_core; then
        cp "$BSF_CORE_DEV_COMPOSE_TEMPLATE" "$BSF_CORE_DEV_COMPOSE"
    fi
}

configure_service() {
    if [[ -n "$1" && "$1" == "true" ]]; then
        say "Configuring $2 in dev mode"
        sed -i "/^\s*image:.*$2/d" "$3"
    else
        say "Configuring $2 in normal mode"
        yq eval "del(.services.$2.build)" -i "$3"
        yq eval "del(.services.$2.develop)" -i "$3"
        yq eval "del(.services.$2.labels)" -i "$3"
    fi
}

configure_olip_compose() {
    say "Configuring OLIP compose..."
    configure_service "$HAO_DEV" "hao" "$DEV_COMPOSE"
    configure_service "$OLIP_API_DEV" "olip-api" "$DEV_COMPOSE"
    configure_service "$OLIP_DASHBOARD_DEV" "olip-dashboard" "$DEV_COMPOSE"

    if [ -z "${APPLICATION_INTEGRATION}" ] || [ "${APPLICATION_INTEGRATION}" != "true" ]; then
        sed -i '/APPLICATION_INTEGRATION_VOLUME/d' "$DEV_COMPOSE"
    fi

    say "OLIP compose configured successfully !"
}

configure_bsf_core_compose() {
    say "Configuring BSF core compose..."
    configure_service "$MAESTRO_SOCKET_DEV" "maestro-socket" "$BSF_CORE_DEV_COMPOSE"
    configure_service "$RPI_CSV_STATS_DEV" "rpi-csv-stats" "$BSF_CORE_DEV_COMPOSE"
    say "BSF core compose configured successfully !"
}

configure_dev_composes() {
    configure_olip_compose
    if is_bsf_core; then
        configure_bsf_core_compose
    fi
}

run_compose() {
    bsf_core_base_command=""
    if [[ -n "$BSF_CORE" && "$BSF_CORE" == "true" ]]; then
        bsf_core_base_command="-f $BSF_CORE_DEV_COMPOSE "
    fi

    base_command="docker compose -f $DEV_COMPOSE $bsf_core_base_command--env-file $MAIN_ENVIRONMENT_FILE --env-file $DEV_ENVIRONMENT_FILE"

    if [[ "$COMMAND" == "up" ]]; then
        $base_command pull
        $base_command up --build --watch --force-recreate
    fi

    if [[ "$COMMAND" == "down" ]]; then
        $base_command down
    fi
}

check_sudo
get_inside_deploy_repository
parse_arguments "$@"
source_env_files
remove_s3fs_bucket
copy_dev_compose_templates
configure_dev_composes
run_compose