#!/bin/bash

set -eu

TAG=${TAG:-latest}
DEV=${DEV:-false}
VM=${VM:-false}
STANDALONE=${STANDALONE:-false}
OLIP_FILES_PATH=${OLIP_FILES_PATH:-/olip-files}
COMPOSES_TO_PULL=("ideascube.compose.yml" "bsf-core.override.compose.yml" "templates/templates.compose.yml" "templates/bsf-core.templates.compose.yml")

SKIP_PULL="false"
SKIP_CONFIG="false"

get_user() {
    if [[ -n "${SUDO_USER:-}" ]]; then
        echo "$SUDO_USER"
    else
        whoami
    fi
}

USER=$(get_user)


say() {
    echo "$1"
}

check_sudo() {
    if [[ "$(id -u)" -ne 0 ]]; then
        say "This script must be run with sudo."
        exit 1
    fi
}

show_help() {
    cat << EOF
Usage: $0 [-t|--tag TAG] [-d|--dev] [--skip-pull] [--vm] [--standalone] [--skip-config] [-o|--olip-files] [-h|--help]
Example: $0 -h

Options:
  -t, --tag           Target tag to deploy (default: latest)
  -d, --dev           Install for dev purposes
  -o, --olip-files    Specify olip-files path
  --vm                Install for VM purposes
  --standalone        Install for standalone purposes
  --skip-pull         Skip pulling compose images
  --skip-config       Skip configuration
  -h, --help          Display this help message
EOF
    exit 0
}

parse_arguments() {
    while [[ $# -gt 0 ]]; do
        case "$1" in
            -t|--tag)
                TAG="$2"
                if [[ "$TAG" != "latest" && "$TAG" != "staging" ]]; then
                    say "Error: tag must be 'latest' or 'staging'"
                    exit 1
                fi
                shift 2
                ;;
            -o|--olip-files)
                OLIP_FILES_PATH="$2"
                shift 2
                ;;
            -d|--dev)
                DEV="true"
                shift
                ;;
            -b|--branch)
                BRANCH="true"
                shift
                ;;
            --skip-pull)
                SKIP_PULL="true"
                shift
                ;;
            --skip-config)
                SKIP_CONFIG="true"
                shift
                ;;
            --vm)
                VM="true"
                shift
                ;;
            --standalone)
                STANDALONE="true"
                shift
                ;;
            -h|--help)
                show_help
                ;;
            *)
                say "Error: invalid option \"$1\""
                exit 1
                ;;
        esac
    done
}

get_inside_deploy_repository() {
    local script_path
    script_path=$(dirname "$(realpath "$0")")
    local deploy_path
    deploy_path=$(realpath "$script_path/..")
    say "Changing directory to \"$deploy_path\""
    cd "$deploy_path" || exit 1
}

source_env_files() {
    # Source $INSTALL_COMPOSES variable
    # shellcheck source=/dev/null
    source constants/install-composes
}

write_olip_files_tag() {
    say "Writing tag to ${OLIP_FILES_PATH}/install/tag"
    mkdir -p "${OLIP_FILES_PATH}/install"
    echo "$TAG" > "${OLIP_FILES_PATH}/install/tag"
}

write_olip_files_deploy_branch() {
    say "Writing deploy branch to ${OLIP_FILES_PATH}/install/deploy-branch"
    mkdir -p "${OLIP_FILES_PATH}/install"
    git rev-parse --abbrev-ref HEAD > "${OLIP_FILES_PATH}/install/deploy-branch"
}

get_compose_services() {
    yq e ".services | to_entries | map(select(.value.profiles == null)) | .[].key" "$1"
}

copy_env_files() {
    local env_files=()
    declare -A standalone_vars

    # If .standalone.env exists, load its variables into an associative array
    if [[ -f "envs/.standalone.env" ]]; then
        while IFS='=' read -r key value; do
            if [[ -n "$key" && "$key" != \#* ]]; then
                standalone_vars["$key"]="$value"
                echo "$key=$value"
            fi
        done < envs/.standalone.env
    fi

    while IFS= read -r -d '' example_file; do
        local target_file="${example_file%.example}"

        # Special handling for .standalone.env.example
        if [[ "$example_file" == "./envs/.standalone.env.example" ]]; then
            cp "$example_file" "$target_file"

            # Fill the new .standalone.env with old values if available
            while IFS='=' read -r key value; do
                if [[ -n "$key" && "$key" != \#* ]]; then
                    value="${standalone_vars[$key]:-$value}" # Use old value if exists, else default
                    sed -i "s|^$key=.*|$key=$value|" "$target_file"
                fi
            done < "$example_file"
        else
            cp "$example_file" "$target_file"
        fi

        env_files+=("$target_file")
    done < <(find . -type f \( -name "*.env.example" -o -name "*.conf.example" \) -print0)

    if [[ "$DEV" != "true" ]]; then
        : > envs/.dev.env
    fi

    if [[ "$STANDALONE" == "false" ]]; then
        rm envs/.standalone.env
        touch envs/.standalone.env
    fi

    printf "%s\n" "${env_files[@]}"
}

copy_conf_files() {
    for example_conf_file in $(find . -type f -name "*.conf.example"); do
        target_conf_file="${example_conf_file%.conf.example}.conf"
        cp "$example_conf_file" "$target_conf_file"
    done
}

configure_env_file() {
    say "Configuring $1"
    local maestro_api_url
    maestro_api_url=$(grep "$TAG" constants/maestro-api-url | cut -d= -f2)
    sed -i "s|MAESTRO_API_URL=.*|MAESTRO_API_URL=$maestro_api_url|" "$1"
    sed -i "s|WEBSOCKET_SERVER_URL=.*|WEBSOCKET_SERVER_URL=$maestro_api_url|" "$1"
    sed -i "s|OLIP_SERVICES_TAG=.*|OLIP_SERVICES_TAG=$TAG|" "$1"
}

copy_and_configure_env_files() {
    if [[ "$SKIP_CONFIG" == "true" ]]; then
        say "Skip copy and configure env files"
        return
    fi

    say "Copying conf files"
    copy_conf_files

    say "Copying env files"
    local env_files
    env_files=$(copy_env_files)

    say "Configuring env files"
    for env_file_to_configure in $env_files; do
        configure_env_file "$env_file_to_configure"
    done
}

pull_compose_images() {
    while IFS= read -r image; do
        if [[ -z "$image" || "$image" == "null" ]]; then
            say "Skipping invalid image: '$image'"
            continue
        fi

        OLIP_SERVICES_TAG="${TAG:-$OLIP_SERVICES_TAG}"
        eval "image_with_tag=\"$image\""
        docker pull "$image_with_tag" || {
            say "Failed to pull image: $image_with_tag"
            exit 1
        }
    done < <(yq e '.services[].image | select(. != null)' "$1")
}

compose_pull() {
    if [[ "$SKIP_PULL" == "true" ]]; then
        say "Skip pulling images"
        return
    fi

    if [[ "$VM" == "true" ]]; then
        say "Pulling VM images"
        pull_compose_images "composes/vm.compose.yml"
        return    
    fi

    if [[ "$STANDALONE" == "true" ]]; then
        say "Pulling standalone images"
        pull_compose_images "composes/standalone.compose.yml"
        return    
    fi

    say "Pre-pulling images for OLIP deploy and install stack"
    for compose_to_pull in "${COMPOSES_TO_PULL[@]}"; do
        pull_compose_images "composes/$compose_to_pull"
    done
}

compose_up() {
    if [[ "$SKIP_CONFIG" == "true" ]]; then
        say "Skip compose up"
        return
    fi
    if [[ "$VM" == "true" ]]; then
        say "Skip compose up"
        return
    fi
    if [[ "$STANDALONE" == "true" ]]; then
        say "Skip compose up"
        return
    fi
    if [[ "$DEV" == "true" ]]; then
        say "Skip compose up"
        return
    fi

    say "Starting OLIP install stack"
    local compose_files=()
    for compose_file in "${INSTALL_COMPOSES[@]}"; do
        compose_files+=("-f" "composes/$compose_file")
    done
    docker compose "${compose_files[@]}" --env-file envs/.env up -d --force-recreate
    say "OLIP installed successfully!"
}

check_sudo
parse_arguments "$@"
get_inside_deploy_repository
source_env_files
write_olip_files_tag
write_olip_files_deploy_branch
copy_and_configure_env_files
compose_pull
compose_up

exit 0