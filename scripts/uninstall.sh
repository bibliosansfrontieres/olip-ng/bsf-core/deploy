#!/bin/bash

OLIP_FILES_PATH=${OLIP_FILES_PATH:-/olip-files}

set -euo pipefail

say() {
    echo "$1"
}

show_help() {
    echo "Usage: $0 [-o|--olip-files]"
    echo "Example: $0 -o /olip-files"
    echo
    echo "Options:"
    echo "  -h, --help          Display this help message"
    echo "  -o, --olip-files    Specify olip-files path"
    exit 0
}

parse_arguments() {
    while [ $# -gt 0 ]; do
        case "$1" in
            -o|--olip-files)
                OLIP_FILES_PATH="$2"
                shift 2
                ;;
            -h|--help)
                show_help
                ;;
            *)
                echo "Invalid option: $1"
                exit 1
                ;;
        esac
    done
}

check_sudo() {
    if [ "$(id -u)" -ne 0 ]; then
        say "This script must be run with sudo."
        exit 1
    fi
}

get_inside_deploy_repository() {
    SCRIPT_PATH=$(dirname "$(realpath "$0")")
    DEPLOY_PATH=$(realpath "$SCRIPT_PATH/..")
    say "Changing directory to \"$DEPLOY_PATH\""
    cd "$DEPLOY_PATH" || exit 1
}

get_env_files() {
    env_files=$(find . -type f -name "*.env")
    echo "$env_files"
}

remove_configuration() {
    say "Removing install configuration"
    backup_directory="/olip-backups/install-$(date +%s)/"
    mkdir -p "$backup_directory"

    say "Removing olip-files"
    target_directory="$backup_directory/olip-files/"
    mkdir -p "$target_directory"
    cp -r ${OLIP_FILES_PATH}/* "$target_directory"
    rm -rf "${OLIP_FILES_PATH}"

    say "Removing env variables"
    target_directory="$backup_directory/envs/"
    mkdir -p "$target_directory"
    for env_file in $(get_env_files); do
        target_file="$target_directory$(basename "$env_file")"

        if [ -f "$target_file" ]; then
            rm -f "$target_file"
        fi

        mv -f "$env_file" "$target_file"
    done
}

# TODO : optionally down hao/maestro-socket

parse_arguments "$@"
check_sudo
get_inside_deploy_repository
remove_configuration
exit 0