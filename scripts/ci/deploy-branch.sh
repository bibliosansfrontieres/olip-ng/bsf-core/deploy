#!/bin/bash

BRANCHES_PATH="${BRANCHES_PATH:-/home/admin/branches}"
CONTAINER="${CONTAINER:-$1}"
OLIP_API_TAG="${OLIP_API_TAG:-staging}"
OLIP_DASHBOARD_TAG="${OLIP_DASHBOARD_TAG:-staging}"
BRANCH_PATH="${BRANCHES_PATH}/${NORMALIZED_LOWERCASE_BRANCH_NAME}"
DEPLOY_BRANCH="${DEPLOY_BRANCH:-main}"
CI_COMMIT_BRANCH="${CI_COMMIT_BRANCH:-main}"

say() {
    echo $1
}

check_sudo() {
    if [[ "$(id -u)" -ne 0 ]]; then
        say "This script must be run with sudo."
        exit 1
    fi
}

get_inside_deploy_repository() {
    SCRIPT_PATH="$(dirname "$(realpath "$0")")"
    DEPLOY_PATH="$(realpath "$SCRIPT_PATH/../..")"
    say "Changing directory to \"$DEPLOY_PATH\""
    cd "$DEPLOY_PATH" || exit 1
}

get_olip_host() {
    echo "Get OLIP host..."
    OLIP_HOST=$(grep -oP '^OLIP_HOST=\K.*' ./envs/.standalone.env)
    echo "OLIP host : ${OLIP_HOST}"
}

clone_deploy_repository() {
    echo "Clone deploy repository..."
    if [ ! -d "${BRANCH_PATH}" ]; then
        say "Cloning deploy repository"
        mkdir -p "${BRANCH_PATH}"
        git clone --branch "${DEPLOY_BRANCH}" https://gitlab.com/bibliosansfrontieres/olip-ng/bsf-core/deploy.git "${BRANCH_PATH}"
        cd "${BRANCH_PATH}" || exit 1
    else
        say "Updating deploy repository"
        cd "${BRANCH_PATH}" || exit 1
        git pull
    fi
}

generate_paths() {
    mkdir -p "${BRANCH_PATH}/data/olip-files"
    mkdir -p "${BRANCH_PATH}/data/olip"
}

uninstall_olip() {
    say "Uninstalling OLIP"
    bash ./scripts/uninstall.sh --olip-files "${BRANCH_PATH}/data/olip-files"
}

install_olip() {
    say "Installing OLIP"

    TAG="main"
    if [[ "$CI_COMMIT_BRANCH" != "main" ]]; then
        TAG="staging"
    fi

    bash ./scripts/install.sh --skip-pull --standalone --tag "${TAG}" --olip-files "${BRANCH_PATH}/data/olip-files"
}

configure_tags() {
    if [[ "$CONTAINER" == "olip-api" ]]; then
        say "Configuring OLIP API tag = ${CI_COMMIT_BRANCH}"
        export OLIP_API_TAG="${CI_COMMIT_BRANCH}"
    fi
    if [[ "$CONTAINER" == "olip-dashboard" ]]; then
        say "Configuring OLIP dashboard tag = ${CI_COMMIT_BRANCH}"
        export OLIP_DASHBOARD_TAG="${CI_COMMIT_BRANCH}"
    fi
}

configure_env_files() {
    say "Configuring env files"
    OLIP_HOST="${BRANCH_HASH}.${OLIP_HOST}"
    VM_ID="${BRANCH_HASH}"
    OLIP_VOLUME="${DEPLOY_PATH}"
    file="./envs/.standalone.env"

    # Read the file line by line and replace variables
    while IFS= read -r line; do
        # Skip empty lines or lines that start with #
        if [[ -z $line || $line == \#* ]]; then
            continue
        fi

        # Extract the key (left of =)
        key=$(echo "$line" | cut -d= -f1)

        # Get the value from the environment
        value=$(eval echo "\$$key")

        if [[ -n $value ]]; then
            # Replace the line with the updated key=value
            sed -i "s|^$key=.*|$key=$value|" "$file"
        else
            # Uncomment one of the following lines based on desired behavior:

            # (1) Leave original value untouched:
            echo "Warning: No value found for variable $key; leaving original value."

            # (2) Remove the line if no value exists:
            # sed -i "/^$key=/d" "$file"
            # echo "Info: Removed $key from $file due to missing value."
        fi
    done < "$file"
    VITE_AXIOS_BASEURL="https://api.${OLIP_HOST}/"
    sed -i "s|^VITE_AXIOS_BASEURL=.*|VITE_AXIOS_BASEURL=$VITE_AXIOS_BASEURL|" "./envs/.olip-dashboard.env"
}

start_branch() {
    echo "Start branch"
    bash ./scripts/deploy.sh --vm-save-id "$VM_SAVE_ID" --branch --olip-files "${BRANCH_PATH}/data/olip-files" --olip-data "${BRANCH_PATH}/data/olip"
}

show_branch_url() {
    echo "Branch URL: https://${OLIP_HOST}"
}

check_sudo
get_inside_deploy_repository
get_olip_host
clone_deploy_repository
generate_paths
uninstall_olip
install_olip
configure_tags
configure_env_files
start_branch
show_branch_url